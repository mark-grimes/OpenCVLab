#
# CMake file to build OpenCVLab.
# Mark Grimes (mark.grimes@rymapt.com)
# 06/Oct/2017
# copyright 2018 Rymapt Ltd
# Licence to be decided
#

project( OpenCVLab )

cmake_minimum_required(VERSION 2.8)

if( NOT MSVC ) # Microsoft Visual Studio is C++11 by default and doesn't recognise this flag
	add_definitions( "-std=c++11" )
endif()


# Create the file that has the version information and git hash
execute_process( COMMAND git describe --dirty --always --tags WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} OUTPUT_VARIABLE OpenCVLab_GIT_DESCRIBE OUTPUT_STRIP_TRAILING_WHITESPACE )
execute_process( COMMAND git log -1 --pretty=format:%H WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} OUTPUT_VARIABLE OpenCVLab_GIT_HASH OUTPUT_STRIP_TRAILING_WHITESPACE )
add_custom_target( ${PROJECT_NAME}_CreateVersionFile ALL ${CMAKE_COMMAND} -E touch "${CMAKE_CURRENT_SOURCE_DIR}/src/version.cpp.in" ) # Make sure the git hash is always checked (not just at configure time)
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/src/version.cpp.in" "${PROJECT_BINARY_DIR}/src/version.cpp" )


include_directories( "${CMAKE_CURRENT_SOURCE_DIR}/include" )
aux_source_directory( "${CMAKE_CURRENT_SOURCE_DIR}/src" library_sources )
list( REMOVE_ITEM library_sources "${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp" ) # main.cpp belongs only in the executable

add_library( ${PROJECT_NAME}_LIB STATIC ${library_sources} "${PROJECT_BINARY_DIR}/src/version.cpp" )
add_executable( ${PROJECT_NAME} "${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp" )
target_link_libraries( ${PROJECT_NAME} ${PROJECT_NAME}_LIB )
## Uncomment this if you get "undefined reference to `pthread_create'" when using std::thread
#if( ${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
#	target_link_libraries( ${PROJECT_NAME}_LIB "pthread" )
#endif()

# Find the OpenCV dependency
find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
target_link_libraries( ${PROJECT_NAME}_LIB ${OpenCV_LIBS} )

#find_package( Eigen REQUIRED )
if( Eigen_DIR )
	include_directories( ${Eigen_DIR} )
else()
	message( STATUS "Eigen_DIR not set, so not adding Eigen include directories" )
endif()

install( TARGETS ${PROJECT_NAME}_LIB DESTINATION lib )
install( DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/include/" DESTINATION include )

option( BUILD_UNITTESTS "Build unit tests" ON )
message( STATUS "BUILD_UNITTESTS: ${BUILD_UNITTESTS}" )
if( BUILD_UNITTESTS )
	# Fix the test configuration file to have the correct paths
	configure_file( "${PROJECT_SOURCE_DIR}/tests/src/testinputs.cpp.in" "${PROJECT_BINARY_DIR}/tests/src/testinputs.cpp" @ONLY )
	include_directories( "${CMAKE_CURRENT_SOURCE_DIR}/tests/include" )
	aux_source_directory( "${CMAKE_CURRENT_SOURCE_DIR}/tests/src" unittests_sources )
	if( NOT CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR )
		# This is a subproject, so just create a list of sources that the parent can use if it wants
		list( REMOVE_ITEM unittests_sources "${CMAKE_CURRENT_SOURCE_DIR}/tests/src/main.cpp" )
		set( ${PROJECT_NAME}_TESTS_SOURCE ${unittests_sources} PARENT_SCOPE )
	else()
		add_executable( ${PROJECT_NAME}Tests ${unittests_sources} "${PROJECT_BINARY_DIR}/tests/src/testinputs.cpp" )
		target_link_libraries( ${PROJECT_NAME}Tests ${PROJECT_NAME}_LIB )
	endif()
endif()
