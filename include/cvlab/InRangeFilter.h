/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include "cvlab/Filter.h"
#include "cvlab/Parameter.h"
#include <opencv2/core/Mat.hpp>

namespace cvlab
{
	/** @brief Filter that uses the cv::inRange function to cut on an image
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	class InRangeFilter : public cvlab::Filter
	{
	public:
		InRangeFilter();
		virtual ~InRangeFilter();

		virtual void process( const cv::Mat& input ) const;
	protected:
		cvlab::Parameter<int> value1Low_;
		cvlab::Parameter<int> value1High_;
		cvlab::Parameter<int> value2Low_;
		cvlab::Parameter<int> value2High_;
		cvlab::Parameter<int> value3Low_;
		cvlab::Parameter<int> value3High_;
		mutable cv::Mat lastInput_;
		void processLastInput() const;
		void parameterCallback( cvlab::Parameter<int>* pOrigin ) const;
	}; // end of class InRangeFilter
} // end of namespace cvlab
