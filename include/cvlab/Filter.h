/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include "cvlab/PipelineStep.h"
#include "cvlab/Processor.h"
//
// Forward declarations
//
namespace cv
{
	class Mat;
}

namespace cvlab
{
	/** @brief Abstract base class for something that filters OpenCV image data
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	class Filter : public cvlab::PipelineStep, public cvlab::Processor
	{
	public:
		virtual ~Filter() {}
	}; // end of class Filter
} // end of namespace cvlab
