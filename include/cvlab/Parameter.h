/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include <string>
#include <utility>
#include <functional>

namespace cvlab
{
	/** @brief Class to represent changeable values in Filter classes.
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	template<typename T>
	class Parameter
	{
	public:
		template<typename name_type>
		Parameter( name_type&& name );

		template<typename name_type,typename value_type>
		Parameter( name_type&& name, value_type&& initialValue );

		template<typename name_type,typename value_type,typename min_type,typename max_type>
		Parameter( name_type&& name, value_type&& initialValue, min_type&& minimum, max_type&& maximum );

		Parameter( const Parameter& other ) = delete;
		Parameter( Parameter&& other ) = default;
		Parameter& operator=( const Parameter& other ) = delete;
		Parameter& operator=( Parameter&& other ) = default;

		const std::string& name() const;
		const T& getMin() const;
		const T& getMax() const;

		const T& get() const;
		bool set( const T& value );
		bool set( T&& value );

		void clearSubscribers();
		void subscribe( std::function<void(cvlab::Parameter<T>* pOrigin)> callback );
	protected:
		T value_;
		T min_;
		T max_;
		std::string name_;
		std::vector<std::function<void(cvlab::Parameter<T>* pOrigin)>> callbacks_;
	}; // end of class Parameter
} // end of namespace cvlab

template<typename T>
template<typename name_type> cvlab::Parameter<T>::Parameter( name_type&& name )
	: min_ ( std::numeric_limits<T>::min() ),
	  max_ ( std::numeric_limits<T>::max() ),
	  name_( std::forward<name_type>(name) )
{
	// No operation besides initialiser list
}

template<typename T>
template<typename name_type,typename value_type>
cvlab::Parameter<T>::Parameter( name_type&& name, value_type&& initialValue )
	: value_( std::forward<value_type>(initialValue) ),
	  min_  ( std::numeric_limits<T>::min() ),
	  max_  ( std::numeric_limits<T>::max() ),
	  name_ ( std::forward<name_type>(name) )
{
	// No operation besides initialiser list
}

template<typename T>
template<typename name_type,typename value_type,typename min_type,typename max_type>
cvlab::Parameter<T>::Parameter( name_type&& name, value_type&& initialValue, min_type&& minimum, max_type&& maximum )
	: value_( std::forward<value_type>(initialValue) ),
	  min_  ( std::forward<min_type>  (minimum) ),
	  max_  ( std::forward<max_type>  (maximum) ),
	  name_ ( std::forward<name_type> (name) )
{
	// No operation besides initialiser list
}

template<typename T>
const std::string& cvlab::Parameter<T>::name() const
{
	return name_;
}

template<typename T>
const T& cvlab::Parameter<T>::getMin() const
{
	return min_;
}

template<typename T>
const T& cvlab::Parameter<T>::getMax() const
{
	return max_;
}

template<typename T>
const T& cvlab::Parameter<T>::get() const
{
	return value_;
}

template<typename T>
bool cvlab::Parameter<T>::set( const T& value )
{
	if( value<min_ || value>max_ ) return false;

	value_=value;
	for( const auto& callback : callbacks_ ) callback( this );
	return true;
}

template<typename T>
bool cvlab::Parameter<T>::set( T&& value )
{
	if( value<min_ || value>max_ ) return false;

	value_=std::move(value);
	for( const auto& callback : callbacks_ ) callback( this );
	return true;
}

template<typename T>
void cvlab::Parameter<T>::clearSubscribers()
{
	callbacks_.clear();
}

template<typename T>
void cvlab::Parameter<T>::subscribe( std::function<void(cvlab::Parameter<T>* pOrigin)> callback )
{
	callbacks_.push_back( std::move(callback) );
}
