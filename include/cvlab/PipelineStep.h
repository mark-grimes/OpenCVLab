/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include <vector>
#include "cvlab/Parameter.h"
//
// Forward declarations
//
namespace cv
{
	class Mat;
}

namespace cvlab
{
	// Forward declare the cvlab::Processor class
	class Processor;

	/** @brief Base class for elements in the image processing pipeline, including sources.
	 *
	 * To implement a step in the pipeline, inherit from this class and call sendResult() when you
	 * have data to send down the pipeline.
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	class PipelineStep
	{
	public:
		virtual ~PipelineStep() {}
		void clearNextSteps();
		void addNextStep( cvlab::Processor& processor );
		const std::vector<cvlab::Parameter<int>*>& getParameters();
	protected:
		void sendResult( const cv::Mat& input ) const;
		void registerParameter( cvlab::Parameter<int>& param );
	private:
		std::vector<const cvlab::Processor*> nextSteps_;
		std::vector<cvlab::Parameter<int>*> parameters_;
	}; // end of class PipelineStep

} // end of namespace cvlab
