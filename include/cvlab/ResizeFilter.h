/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include "cvlab/Filter.h"
#include "cvlab/Parameter.h"
#include <opencv2/core/Mat.hpp>

namespace cvlab
{
	/** @brief Filter that uses the cv::inRange function to cut on an image
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	class ResizeFilter : public cvlab::Filter
	{
	public:
		ResizeFilter();
		virtual ~ResizeFilter();

		virtual void process( const cv::Mat& input ) const;

		void setScaling( float scale );
	protected:
		cvlab::Parameter<int> scale_;
		mutable cv::Mat lastInput_;
		void processLastInput() const;
		void parameterCallback( cvlab::Parameter<int>* pOrigin ) const;
	}; // end of class ResizeFilter
} // end of namespace cvlab
