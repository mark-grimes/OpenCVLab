/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
//
// Forward declarations
//
namespace cv
{
	class Mat;
}

namespace cvlab
{
	/** @brief Abstract base class for something that takes a cv::Mat as an input.
	 *
	 * Filters and GUIs inherit from this class.
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	class Processor
	{
	public:
		virtual ~Processor() {}
		virtual void process( const cv::Mat& input ) const = 0;
	}; // end of class Processor
} // end of namespace cvlab
