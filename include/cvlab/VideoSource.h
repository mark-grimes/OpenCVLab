/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include "cvlab/PipelineStep.h"
#include <opencv2/highgui.hpp>

namespace cvlab
{
	class VideoSource : public cvlab::PipelineStep
	{
	public:
		VideoSource( const cvlab::VideoSource& other ) = default;
		VideoSource( cvlab::VideoSource&& other ) = default;
		VideoSource& operator=( const cvlab::VideoSource& other ) = default;
		VideoSource& operator=( cvlab::VideoSource&& other ) = default;

		template<typename... Ts>
		VideoSource( Ts&&... args );
		virtual ~VideoSource() {}

		bool nextFrame();
	protected:
		cv::VideoCapture capture_;
	};
} // end of namespace cvlab

template<typename... Ts>
cvlab::VideoSource::VideoSource( Ts&&... args )
	: capture_( std::forward<Ts...>( args...) )
{
	// No operation besides initaliser list
}
