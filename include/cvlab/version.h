/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#ifndef INCLUDEGUARD_cvlab_version_h
#define INCLUDEGUARD_cvlab_version_h

#include <iosfwd>

namespace cvlab
{
	namespace version
	{
		extern const char* GitDescribe;
		extern const char GitHash[41];

	} // end of namespace version
} // end of namespace cvlab

#endif
