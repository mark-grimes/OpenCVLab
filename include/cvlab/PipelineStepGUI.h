/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#pragma once
#include "cvlab/Processor.h"
#include <string>

namespace cvlab
{
	// Forward declare the cvlab::Processor class
	class PipelineStep;

	/** @brief
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 24/Jun/2018
	 * @copyright Copyright 2018 Rymapt Ltd. Licence to be decided.
	 */
	class PipelineStepGUI : protected cvlab::Processor
	{
	public:
		PipelineStepGUI();
		PipelineStepGUI( const cvlab::PipelineStepGUI& other ) = delete;
		PipelineStepGUI( cvlab::PipelineStepGUI&& other ) = delete;
		PipelineStepGUI& operator=( const cvlab::PipelineStepGUI& other ) = delete;
		PipelineStepGUI& operator=( cvlab::PipelineStepGUI&& other ) = delete;
		virtual ~PipelineStepGUI();

		void setSource( cvlab::PipelineStep& step );
	protected:
		virtual void process( const cv::Mat& input ) const override;
		std::string windowName_;
		int initialValue_; // Need an integer address to pass as initial value (same one used for all trackbars)
		static void trackbarCallback( int value, void* pUserData );
	}; // end of class PipelineStepGUI

} // end of namespace cvlab
