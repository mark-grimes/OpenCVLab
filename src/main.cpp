/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include <iostream>
#include "cvlab/version.h"
#include <opencv2/highgui.hpp>
#include "cvlab/VideoSource.h"
#include "cvlab/PipelineStepGUI.h"
#include "cvlab/InRangeFilter.h"
#include "cvlab/ResizeFilter.h"

namespace // Use the unnamed namespace for things only used in this file
{
	/* Simple analysis of command line arguments to create a capture source from a file or camera */
	cvlab::VideoSource createCapture( int argc, char* argv[] );
} // end of the unnamed namespace

int main( int argc, char* argv[] )
{
	std::cout << "OpenCVLab program with version " << cvlab::version::GitDescribe << "\n";

	cvlab::VideoSource capture=createCapture( argc, argv );
	cvlab::ResizeFilter halveSize;
	cvlab::InRangeFilter inRange;

	capture.addNextStep( halveSize );
	halveSize.addNextStep( inRange );

	halveSize.setScaling(0.5);

	cvlab::PipelineStepGUI originalGUI;
	cvlab::PipelineStepGUI inRangeGUI;
	originalGUI.setSource( halveSize );
	inRangeGUI.setSource( inRange );


	capture.nextFrame();

	while( true )
	{
		char key = (char) cv::waitKey(0);
		if( key == 'q' || key == 27 ) break;
		else if( key=='n' ) capture.nextFrame();
		else std::cout << "Unknown key " << key << std::endl;
	}

	return 0;
}

namespace // Unnamed namespace
{
	cvlab::VideoSource createCapture( int argc, char* argv[] )
	{
		if( argc==1 )
		{
			std::cout << "Trying to open the default video capture device" << std::endl;
			return {0};
		}
		else if( argc==2 )
		{
			std::cout << "Trying to open video file '" << argv[1] << "'" << std::endl;
			return {argv[1]};
		}
		else throw std::invalid_argument("Incorrect number of command line arguments");
	}
} // end of the unnamed namespace
