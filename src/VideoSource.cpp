/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/VideoSource.h"

bool cvlab::VideoSource::nextFrame()
{
	cv::Mat capturedFrame;
	if( capture_.read(capturedFrame) )
	{
		this->sendResult( capturedFrame );
		return true;
	}
	else return false;
}
