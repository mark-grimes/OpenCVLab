/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/ResizeFilter.h"
#include "cvlab/PipelineStep.h"
#include <opencv2/opencv.hpp>

cvlab::ResizeFilter::ResizeFilter()
	: scale_("Scale", 10, 1, 20 )
{
	scale_.subscribe( std::bind( &ResizeFilter::parameterCallback, this, std::placeholders::_1 ) );
	this->registerParameter( scale_ );
}

cvlab::ResizeFilter::~ResizeFilter()
{
	// No operation
}

void cvlab::ResizeFilter::process( const cv::Mat& input ) const
{
	// Note that this creates a reference to the original, so no data is copied. See
	// https://docs.opencv.org/3.4/d3/d63/classcv_1_1Mat.html#a294eaf8a95d2f9c7be19ff594d06278e
	lastInput_=input;
	processLastInput();
}

void cvlab::ResizeFilter::setScaling( float scale )
{
	scale_.set( scale*10.0 );
}

void cvlab::ResizeFilter::processLastInput() const
{
	if( lastInput_.size().width==0 && lastInput_.size().height==0 ) return;

	cv::Mat output;
	float scale=static_cast<float>( scale_.get() )/10.0;
	cv::resize( lastInput_, output, cv::Size(0,0), scale, scale );
	this->sendResult( output );
}


void cvlab::ResizeFilter::parameterCallback( cvlab::Parameter<int>* pOrigin ) const
{
	// Note that the pOrigin parameter is ignored, processLastInput queries every parameter anyway
	processLastInput();
}
