/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/PipelineStepGUI.h"
#include "cvlab/PipelineStep.h"
#include <opencv2/highgui.hpp>

cvlab::PipelineStepGUI::PipelineStepGUI()
{
	// No operation
}

cvlab::PipelineStepGUI::~PipelineStepGUI()
{
	if( !windowName_.empty() ) cv::destroyWindow( windowName_.c_str() );
}

void cvlab::PipelineStepGUI::setSource( cvlab::PipelineStep& step )
{
	//
	// Release any sources from before
	//
	if( windowName_.empty() )
	{
		windowName_=std::to_string(reinterpret_cast<long>(this)); // Use the address as a unique addentifier
		cv::namedWindow( windowName_ );
	}

	//
	// Create sliders for any parameters the step has
	//
	for( auto& pParameter : step.getParameters() )
	{
		// Need to shift so the minimum is at zero
		initialValue_=pParameter->get()-pParameter->getMin();
		cv::createTrackbar( pParameter->name(), windowName_, &initialValue_, pParameter->getMax()-pParameter->getMin(), &PipelineStepGUI::trackbarCallback, pParameter );
	}

	//
	// Finally tell the step to inform us when it gets updated
	//
	step.addNextStep( *this );
}

void cvlab::PipelineStepGUI::process( const cv::Mat& input ) const
{
	if( !windowName_.empty() )
	{
		cv::imshow( windowName_, input );
	}
}

void cvlab::PipelineStepGUI::trackbarCallback( int value, void* pUserData )
{
	cvlab::Parameter<int>* pParameter=reinterpret_cast<cvlab::Parameter<int>*>(pUserData);
	// value always starts at zero, so need to add offset
	bool result=pParameter->set( pParameter->getMin()+value );
}
