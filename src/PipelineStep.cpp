/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/PipelineStep.h"
#include "cvlab/Processor.h"

void cvlab::PipelineStep::clearNextSteps()
{
	nextSteps_.clear();
}

void cvlab::PipelineStep::addNextStep( cvlab::Processor& processor )
{
	nextSteps_.push_back( &processor );
}

const std::vector<cvlab::Parameter<int>*>& cvlab::PipelineStep::getParameters()
{
	return parameters_;
}

void cvlab::PipelineStep::sendResult( const cv::Mat& result ) const
{
	for( const auto& pProcessor : nextSteps_ ) pProcessor->process( result );
}

void cvlab::PipelineStep::registerParameter( cvlab::Parameter<int>& param )
{
	parameters_.push_back( &param );
}
