/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/InRangeFilter.h"
#include "cvlab/PipelineStep.h"
#include <opencv2/opencv.hpp>

cvlab::InRangeFilter::InRangeFilter()
	: value1Low_ ("Value 1 low", 0  ,0,255),
	  value1High_("Value 1 high",100,0,255),
	  value2Low_ ("Value 2 low", 0  ,0,255),
	  value2High_("Value 2 high",100,0,255),
	  value3Low_ ("Value 3 low", 0  ,0,255),
	  value3High_("Value 3 high",100,0,255)
{
	value1Low_.subscribe( std::bind( &InRangeFilter::parameterCallback, this, std::placeholders::_1 ) );
	value1High_.subscribe( std::bind( &InRangeFilter::parameterCallback, this, std::placeholders::_1 ) );
	value2Low_.subscribe( std::bind( &InRangeFilter::parameterCallback, this, std::placeholders::_1 ) );
	value2High_.subscribe( std::bind( &InRangeFilter::parameterCallback, this, std::placeholders::_1 ) );
	value3Low_.subscribe( std::bind( &InRangeFilter::parameterCallback, this, std::placeholders::_1 ) );
	value3High_.subscribe( std::bind( &InRangeFilter::parameterCallback, this, std::placeholders::_1 ) );

	this->registerParameter( value1Low_ );
	this->registerParameter( value1High_ );
	this->registerParameter( value2Low_ );
	this->registerParameter( value2High_ );
	this->registerParameter( value3Low_ );
	this->registerParameter( value3High_ );
}

cvlab::InRangeFilter::~InRangeFilter()
{
	// No operation
}

void cvlab::InRangeFilter::process( const cv::Mat& input ) const
{
	// Note that this creates a reference to the original, so no data is copied. See
	// https://docs.opencv.org/3.4/d3/d63/classcv_1_1Mat.html#a294eaf8a95d2f9c7be19ff594d06278e
	lastInput_=input;
	processLastInput();
}

void cvlab::InRangeFilter::processLastInput() const
{
	cv::Mat output;
	cv::inRange( lastInput_, cv::Scalar(value1Low_.get(),value2Low_.get(),value3Low_.get()), cv::Scalar(value1High_.get(),value2High_.get(),value3High_.get()), output );
	this->sendResult( output );
}


void cvlab::InRangeFilter::parameterCallback( cvlab::Parameter<int>* pOrigin ) const
{
	// Note that the pOrigin parameter is ignored, processLastInput queries every parameter anyway
	processLastInput();
}
