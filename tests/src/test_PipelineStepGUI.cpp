/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/PipelineStepGUI.h"
#include "cvlab/VideoSource.h"
#include "cvlabtests/testinputs.h"
#include <catch.hpp>

SCENARIO( "Test that cvlab::PipelineStepGUI works as expected", "[PipelineStepGUI]" )
{
	GIVEN( "A PipelineStepGUI instance" )
	{
		cvlab::VideoSource capture( cvlabtests::testinputs::testFileDirectory+"swedenSouthKorea_clip.mp4" );
		cvlab::PipelineStepGUI gui;
		gui.setSource( capture );

		bool callResult;
		REQUIRE_NOTHROW( callResult=capture.nextFrame() );
		REQUIRE( callResult==true );
	} // end of "GIVEN a PipelineStepGUI instance"
} // end of "SCENARIO Test PipelineStepGUI"
