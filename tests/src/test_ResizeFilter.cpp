/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/ResizeFilter.h"
#include "cvlab/VideoSource.h"
#include "cvlabtests/testinputs.h"
#include <catch.hpp>

class TestFilter : public cvlab::Filter
{
public:
	virtual ~TestFilter() {}
	virtual void process( const cv::Mat& input ) const override
	{
		wasCalled=true;
		frameSize=input.size();
		depth=input.depth();
		channels=input.channels();
	}
	mutable bool wasCalled=false;
	mutable cv::Size frameSize;
	mutable int depth;
	mutable int channels;
};

SCENARIO( "Test that cvlab::ResizeFilter works as expected", "[ResizeFilter]" )
{
	GIVEN( "A ResizeFilter instance connected to a VideoSource" )
	{
		cvlab::VideoSource capture( cvlabtests::testinputs::testFileDirectory+"swedenSouthKorea_clip.mp4" );
		cvlab::ResizeFilter resize;
		TestFilter filter;
		capture.addNextStep( resize );
		resize.addNextStep( filter );

		bool callResult;
		REQUIRE_NOTHROW( callResult=capture.nextFrame() );
		REQUIRE( callResult==true );
		REQUIRE( filter.wasCalled==true );
		CHECK( filter.frameSize.width==720 );
		CHECK( filter.frameSize.height==576 );
		CHECK( filter.depth==0 );
		CHECK( filter.channels==3 );

		REQUIRE( resize.getParameters().size()==1 );
		filter.wasCalled=false;
		REQUIRE_NOTHROW( resize.getParameters()[0]->set(5) ); // This should halve the size (scale is X/10.0)
		REQUIRE( filter.wasCalled==true );
		CHECK( filter.frameSize.width==720/2 );
		CHECK( filter.frameSize.height==576/2 );
		CHECK( filter.depth==0 );
		CHECK( filter.channels==3 );
	} // end of "GIVEN a ResizeFilter instance"
} // end of "SCENARIO Test ResizeFilter"
