/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/Parameter.h"
#include <catch.hpp>

SCENARIO( "Test that cvlab::Parameter works as expected", "[Parameter]" )
{
	WHEN( "Using a Parameter<double> instance" )
	{
		cvlab::Parameter<double> testParameter( "test" );
		INFO( "Min=" << testParameter.getMin() << " max=" << testParameter.getMax() );
		REQUIRE_NOTHROW( testParameter.set(4) );
		double value;
		REQUIRE_NOTHROW( value=testParameter.get() );
		REQUIRE( value==4 );
	}
	WHEN( "Checking callbacks are called" )
	{
		double result=-1;
		cvlab::Parameter<double> testParameter( "test" );
		REQUIRE_NOTHROW( testParameter.subscribe( [&](cvlab::Parameter<double>* pOrigin){ result=pOrigin->get(); } ) );
		REQUIRE( result==-1 );
		REQUIRE_NOTHROW( testParameter.set(4) );
		REQUIRE( result==4 );
	}
	WHEN( "Checking Parameter<int> with min and max" )
	{
		int result=-1;
		cvlab::Parameter<int> testParameter( "test", 32, 0, 100 );
		REQUIRE_NOTHROW( testParameter.subscribe( [&](cvlab::Parameter<int>* pOrigin){ result=pOrigin->get(); } ) );
		REQUIRE( result==-1 );
		REQUIRE_NOTHROW( testParameter.set(4) );
		REQUIRE( result==4 );
		REQUIRE( testParameter.get()==4 );
		// These calls shouldn't work because they're outside the limits set
		REQUIRE_NOTHROW( testParameter.set(-5) );
		REQUIRE( result==4 );
		REQUIRE( testParameter.get()==4 );
		REQUIRE_NOTHROW( testParameter.set(101) );
		REQUIRE( result==4 );
		REQUIRE( testParameter.get()==4 );
		// Check again that it will set within the limits
		REQUIRE_NOTHROW( testParameter.set(89) );
		REQUIRE( result==89 );
		REQUIRE( testParameter.get()==89 );
	}
} // end of "SCENARIO Test Parameter"
