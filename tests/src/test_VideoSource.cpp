/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
#include "cvlab/VideoSource.h"
#include "cvlab/Filter.h"
#include "cvlabtests/testinputs.h"
#include <catch.hpp>

class TestFilter : public cvlab::Filter
{
public:
	virtual ~TestFilter() {}
	virtual void process( const cv::Mat& input ) const override
	{
		wasCalled=true;
		frameSize=input.size();
		depth=input.depth();
		channels=input.channels();
	}
	mutable bool wasCalled=false;
	mutable cv::Size frameSize;
	mutable int depth;
	mutable int channels;
};

SCENARIO( "Test that cvlab::VideoSource works as expected", "[VideoSource]" )
{
	GIVEN( "A VideoSource instance" )
	{
		cvlab::VideoSource capture( cvlabtests::testinputs::testFileDirectory+"swedenSouthKorea_clip.mp4" );
		TestFilter filter;
		capture.addNextStep( filter );

		bool callResult;
		REQUIRE_NOTHROW( callResult=capture.nextFrame() );
		REQUIRE( callResult==true );
		REQUIRE( filter.wasCalled==true );
		CHECK( filter.frameSize.width==720 );
		CHECK( filter.frameSize.height==576 );
		CHECK( filter.depth==0 );
		CHECK( filter.channels==3 );
	} // end of "GIVEN a VideoSource instance"
} // end of "SCENARIO Test VideoSource"
