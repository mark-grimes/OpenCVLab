/*
 * Copyright 2018 Rymapt Ltd. Licence to be decided.
 */
/** @file Information required by all tests (locations of input files etcetera). */
#include <string>

namespace cvlabtests
{
	struct testinputs
	{
		static const std::string testFileDirectory;
	};
} // end of namespace cvlabtests
